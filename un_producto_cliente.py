# coding=utf-8
import scrapy
import re
import sys
import string
from scrapy_splash import SplashRequest
import os , subprocess
import json

TOKEN = "APP_USR-3568915230614128-040508-ddce466c978c7e58b4279dab9ebf53d2-23351971"

class PreciosEasy(scrapy.Spider):
    name = "un-producto-cliente"
    
    def start_requests(self):
        urls = [
            #'https://listado.mercadolibre.com.ar/_Desde_151_CustId_228302638_seller*id_228302638',
            #'https://listado.mercadolibre.com.ar/_Desde_101_CustId_228302638_seller*id_228302638',
            #'https://listado.mercadolibre.com.ar/_Desde_51_CustId_228302638_seller*id_228302638',
            #'https://listado.mercadolibre.com.ar/_CustId_228302638',
            #'https://listado.mercadolibre.com.ar/_CustId_23351971',
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    
    def cleanString(self, string_parser):
        value_clean =  re.sub('[^A-Za-z0-9-,]+', ' ', str(string_parser))
        return value_clean
    
    def cleanPrice(self, string_parser):
        value_clean =  re.sub('[^0-9-,-.]+', ' ', str(string_parser))
        return value_clean

    def executeCurl(self, idProd):
        try:
            p = subprocess.Popen('curl -X GET https://api.mercadolibre.com/items/' + str(idProd), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            return out
        except:
            return ''

    def descripcion(self, idProd):
        try:
            p = subprocess.Popen('curl -X GET https://api.mercadolibre.com/items/' + str(idProd) + '/description', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            return out
        except:
            return ''

    def insertProductoMercadolibre(self,info):
        try:
            datos = json.dumps(info)
            data = "curl -X POST -H 'Content-Type: application/json' -d '" + datos + "' https://api.mercadolibre.com/items?access_token=" + TOKEN
            p = subprocess.Popen(data, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            return out
        except:
            return ''

    def pausarProductoMercadolibre(self,info):
        try:
            data = 'curl -X PUT -H "Content-Type: application/json" -H "Accept: application/json" -d {"status":"paused"} https://api.mercadolibre.com/items/' + str(info) + '?access_token=' + TOKEN
            p = subprocess.Popen(data, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            return out
        except:
            return ''

    def writeProductoInfo(self, info):
        with open('ariel.json', 'a+') as outfile:
            json.dump(info, outfile)

    def parse(self, response):  
        try:     
            i = 0 
            #idsProd     =   response.css('#searchResults').xpath('li/div/@id').extract()
	    idsProd = [u'MLA764795367', u'MLA698225908', u'MLA613940046', u'MLA754278985', u'MLA731170183', u'MLA678692411', u'MLA705288364', u'MLA754554978', u'MLA620852953', u'MLA687878902', u'MLA649976089', u'MLA737084607', u'MLA678625173', u'MLA635921864', u'MLA754529852', u'MLA676129389', u'MLA632940529', u'MLA620943608', u'MLA612266767', u'MLA660118472', u'MLA660118786', u'MLA639108117', u'MLA647597767']
            print idsProd
            a = {}
            for idProd in idsProd:
                i = i +1
 	        if i < 2:
                    try:
                        result =  self.executeCurl(idProd)
                        d = json.loads(result)
                        datos = ['seller_address','start_time','base_price', 'original_price', 'descriptions', 'stop_time', 'parent_item_id', 'health', 'differential_pricing', 'thumbnail', 'secure_thumbnail', 'permalink', 'seller_id', 'initial_quantity', 'seller_contact', 'date_created', 'id', 'geolocation', 'deal_ids', 'warnings', 'international_delivery_mode', 'sub_status', 'listing_source', 'subtitle', 'last_updated', 'sold_quantity', 'catalog_product_id']
 		        for dato in datos:
		            del d[dato]                    
                        descripcion = self.descripcion(idProd)
                        descripcion = json.loads(descripcion)
                        descripcion = descripcion['plain_text']
                        descripcion = descripcion[:descripcion.find("UBICACI")]
                        datas = ['Clic De Compras','clic de Compras','clic De Compras','clic de compras','Clic De compras','Clic de Compras','Somos Mercado Líder Platinum (la calificación más alta con la que mercado libre distingue a los mejores vendedores del sitio).']
                        for data in datas:
                            descripcion = descripcion.replace(data,"")
                        descripcion = "\n\nLYAINSUMOS\n\n-----------------------------------------------------------------------------------------------------\n\nAtención:\nPor Favor Consultar Stock antes de Ofertar !\n\n-----------------------------------------------------------------------------------------------------\n\n" + descripcion + "UBICACIÓN & REFERENCIAS.\n\nTambién podes retirar tu compra en nuestro local a 1 cuadra de la estación de trenes de Lomas de Zamora.\n\nTE DEJAN CERCA DE NOSOTROS:\n\nColectivos (Radio aproximado de 5 cuadras): 51, 165, 318, 540, 550, 551, 552.\n Tren Roca (1 Cuadra) :  Estación Lomas de Zamora.\nEstamos a 5 cuadras de la Municipalidad de Lomas de Zamora.\n\n-----------------------------------------------------------------------------------------------------\n\nHORARIOS:\n\nLunes a Viernes 10.30hs - 13hs y 14hs - 18hs\n\nSábados 10.30hs - 13hs"
                        print descripcion 
                        d['description'] = {"plain_text":descripcion}
                        #self.writeProductoInfo(d)
                        if 'variations' in d :
                            if d['variations'] <> []:
                                if 'catalog_product_id' in d['variations'][0]:
			            del  d['variations'][0]['catalog_product_id']
                        print json.dumps(d)
                        salida = self.insertProductoMercadolibre(d)
                        salida = json.loads(salida)
                        print salida
		        print "..............................................",i
		        print "id.......................: ", salida['id']
	                pausar = self.pausarProductoMercadolibre(salida['id'])
                        pausar = json.loads(pausar)
                        #print descripcion
                        a[salida['id']] = idProd
                    except:
                        print "ERROR" 
            yield(idsProd)
            print a
        except:
            print('ERROR')
